# 1.-Conjuntos, Aplicaciones y funciones (2002)
```plantuml 
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
  .orange {
    BackgroundColor #f33501
  }
}
</style>
* Conjuntos, Aplicaciones y funciones
** Conjuntos <<green>>
*** Consiste en <<blue>>
**** Una coleccion de elementos <<rose>>
*** Tipos <<blue>>
**** Universal <<rose>>
*****_ es
****** Conjunto donde ocurre toda la teoria
**** Vacio <<rose>>
*****_ es
****** Cuando no tiene ningun elemento
*** Operaciones <<blue>>
**** Consiste en <<rose>>
***** Crear conjuntos mas complejos \npor medio de conjuntos elementales
**** Basicas <<rose>>
***** Interseccion <<orange>> 
****** Elementos que pertenecen simultaneamente a ambos
***** Union <<orange>>
****** Elementos que pertenecen a almenos uno de ellos
***** Complementacion <<orange>>
****** Elementos que no pertenecen a ninguno
***** Diferencia <<orange>>
****** Elementos del primer conjunto que \nno tiene elementos del segundo 
*** Representacion Grafica <<blue>>
**** Conocidos como <<rose>>
***** Diagramas de Venn
****** Creado por Jhon Venn <<orange>>
****** En 1880 <<orange>>
****** Ayuda a comprender intuitivamente la posición <<orange>>
******_ se representa mediante
******* Circulos <<orange>>
******* Ovalos <<orange>>
*** Inclusión de conjuntos <<blue>>
**** Consiste en <<rose>> 
***** Cuando un conjunto esta dentro de otro <<orange>>
***** Cuando todos los elementos del primero pertenecen al segundo  <<orange>>
*** Cardinal de un conjunto <<blue>>
**** Consiste en <<rose>>
***** El numero de elementos que conforman al conjunto 
**** Segun los elementos pueden tener \nciertas propiedades <<rose>>
***** Formulas para resolver <<orange>>
****** Union de conjuntos 
***** Acotacion de cardinales <<orange>>
****** Mayor que  
****** Menor que 
****** Igual que 
** Aplicacion <<green>>
*** Consiste en <<blue>>
**** Una transformacion que convierte a cada uno de los elementos \n del conjunto en uno unico de un conjunto final <<rose>>
*** Tipos <<blue>>
**** Inyectiva <<rose>>
***** Si 2 elementos tienen la misma imagen entonces son el mismo elemento
**** Sobreyectiva <<rose>>
***** Si es igual
**** Biyectiva <<rose>>
***** Cuando es inyectiva y sobreyectiva
** Funcion <<green>>
*** Grafica de la funcion <<blue>>
**** Serie de puntos <<rose>>
**** Parabolas <<rose>>
**** Linea recta <<rose>>
@endmindmap
```
# 2.-Funciones (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style> 
* Funciones
** Consiste en <<green>>
*** Transformaciones de un conjunto a otro <<blue>>
** Representacion Grafica (Cartesiana) <<green>>
***_ establecida por
**** René Descarter <<blue>>
***_ es
**** Representar en un plano los valores de los \nconjuntos de numeros <<blue>>
***_ mediante 
**** La funcion de una curva en la grafica <<blue>>
*****_ se puede saber
****** Como cambia <<rose>>
****** Como es  <<rose>>
** Tipos <<green>>
*** Pueden ser <<blue>>
**** Crecientes  <<rose>>
*****_ es
****** Cuando aumenta la variable independiente \nde igual manera sus imagenes
**** Decrecientes <<rose>>
*****_ es
***** Cuando disminuye la variable independiente
** Caracteristicas <<green>>
*** Puede ser <<blue>>
**** Continua <<rose>>
***** Tienen buen comportamiento
***** Son manejables
***** No produce saltos ni discontinuidades 
**** Discontinua <<rose>>
***** Puede existir un salto en algun punto
*** Tienen <<blue>>
**** Intervalo <<rose>>
***** Un tramo del rango de los valores \nque se puede tomar
**** Dominio  <<rose>>
***** Conjunto de elementos x de la \nvariable inedpendiente
*** Limite <<blue>>
**** A que valor se aproxima y como
** Calculo diferencial <<green>>
*** La derivada <<blue>>
**** Resuelve el problema \nde la aproximacion <<rose>>
***** Aproximar una funcion complicada \n con una funcion mas simplre
**** Funciones lineales <<rose>>
*****_ consiste en 
****** Una funcion sencilla donde sus lineas en la grafica son rectas 
@endmindmap
```
# 3.-La matemática del computador (2002)
```plantuml 
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* La matemática del computador
** Matematicas <<green>>
***_ es
**** Una ciencia que parte de \nuna deduccion logica <<blue>>
*****_ nos permite
****** Estudiar los valores \nabstractos como son los numeros <<rose>> 
*** Es muy importante para el desarrollo de las computadoras <<blue>>
*** Ayuda a resolver los problemas cotidianos <<blue>>
*** Representación interna <<blue>>
**** De numeros enteros <<rose>>
*****_ tipos
****** Magnitud de signo
****** Exceso
****** Complemento a dos
**** Nos dice  <<rose>>
***** Como convertir un paso de corriente de unos y ceros \n en una serie de posiciones de memoria
*** Aritmetica finita <<blue>>
**** Dijitos significativos <<rose>>
***** Miden la precision general relativa de un valor
**** Truncar un número <<rose>> 
*****_ consiste en 
****** Cortar un numero que tiene infinitos decimales
****** Cortar unas cuantas cifras a la derecha apartir de una dada
**** Redondear un número <<rose>>
***** Evitar errores con los numero
***** Un truncamiento refinado
***** Los errores cometidos sean los menos posibles
** Sistemas de numeracion <<green>>
*** Binario <<blue>>
**** Se usa en computadores y es su logica utilizada <<rose>>
**** Enlaza con la logica booleana <<rose>>
**** Se prepresenta mediante <<rose>>
*****_ numero
****** 1
****** 0
**** Se pueden crear estructuras complejas <<rose>>
*****_ como
****** Letras
****** Videos
****** Imagenes
****** Signos de puntuacion
*** Octal <<blue>>
**** Sistema de numeracion con base 8 <<rose>>
**** No se utilizan simbolos, solo digitos  <<rose>>
****_ se utiliza 
***** En la computadora en lugar del sistema hexadecimal <<rose>>
*** Hexadecimal <<blue>>
**** Sistema de numeracion con base 16 <<rose>>
**** Sistema posicional <<rose>>
@endmindmap
```